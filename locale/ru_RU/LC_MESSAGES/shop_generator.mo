��            )   �      �     �     �     �  #   �      
  +   +  +   W  Z   �  !   �                <     D     Y     m     �     �     �     �     �     �                 @   +  $   l     �     �     �  $  �  1   �  %   "  <   H  =   �  ;   �     �  b   	  �   s	  N    
  <   o
  &   �
  
   �
  0   �
  0     )   @  !   j  	   �  0   �  #   �  @   �  @   ,  
   m     x  )   �  �   �  W   8     �  .   �  #   �                                                            
                              	                                               %02d hr %02d min %02d sec (total time: %s) Ask for technical support Basic settings can be found in the  Category for generating products Check, if you want render image background. Click on the link to contact the developer. Do not close the browser window and not leave the page as long as the process is completed Number of images for each product Number of products generated Prefix the name of the product Product Product image height Product image width Products generation is in the  Products generator QR code Render background Select a category Set the product image height. Set the product image width. Start Successfully Technical support The plugin generates a specified number of products in category. This extension is used for computing Type of products export section generator plugin settings Project-Id-Version: shop/plugins/generator
POT-Creation-Date: 2016-01-05 21:57+0300
PO-Revision-Date: 
Last-Translator: 
Language-Team: shop/plugins/generator <covoxx@gmail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=((((n%10)==1)&&((n%100)!=11))?(0):(((((n%10)>=2)&&((n%10)<=4))&&(((n%100)<10)||((n%100)>=20)))?(1):2));
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: .
Language: ru_RU
X-Generator: Poedit 1.5.4
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: .
 %02d часов %02d минут %02d секунд (время исполнения: %s) Запросить техническую поддержку Основные настройки можно найти в  Категория для генерации товаров Отметьте Перейдите по ссылке, чтобы связаться с разработчиком. Не закрывайте окно браузера и не покидайте страницу до тех пор, пока процесс не будет завершен Количество изображений для каждого товара Количество генерируемых товаров Префикс имени товара Товар Высота изображения товара Ширина изображения товара Товары генерируются в  Генератор товаров QR код Сгенерировать задний план Выберите категорию Задайте высоту изображения товара. Задайте ширину изображения товара. Старт Успешно Техническая поддержка Плагин генерирует определенное количество товаров в заданной категории. Расшрение, которое используется для вычислений Тип товара разделе импорта/экспорта настройках плагина 