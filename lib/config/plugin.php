<?php
return array(
    'name'              => _wp('Products generator'),
    'description'       => _wp('The plugin generates a specified number of products in category.'),
    'icon'              => 'img/generator16.png',
    'img'               => 'img/generator16.png',
    'vendor'            => 964801,
    'version'           => '1.0.2',
    'importexport'      => 1,//'profiles',
    'import_profile'    => false,
    'handlers'          =>
        array(),
);
